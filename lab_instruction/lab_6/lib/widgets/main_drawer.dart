import 'package:flutter/material.dart';

import '../screens/filters_screen.dart';

class MainDrawer extends StatelessWidget {
  Widget buildListTile(String title, IconData icon, Function tapHandler) {
    return ListTile(
      leading: Icon(
        icon,
        size: 26,
      ),
      title: Text(
        title,
        style: TextStyle(
          fontFamily: 'RobotoCondensed',
          fontSize: 24,
          fontWeight: FontWeight.bold,
        ),
      ),
      onTap: tapHandler,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Column(
        children: <Widget>[
          Container(
            height: 120,
            width: double.infinity,
            padding: EdgeInsets.all(20),
            alignment: Alignment.centerLeft,
            color: Color.fromRGBO(89, 165, 216, 1),
            child: Text(
              'Hello, User!',
              style: TextStyle(
                  fontWeight: FontWeight.w900,
                  fontSize: 32,
                  color: Colors.white),
            ),
          ),
          SizedBox(
            height: 20,
          ),
          buildListTile('Home', Icons.roofing, () {
            Navigator.of(context).pushReplacementNamed('/');
          }),
          buildListTile('Donasi', Icons.touch_app, () {
            Navigator.of(context).pushReplacementNamed('/');
          }),
          buildListTile('Publikasi', Icons.topic, () {
            Navigator.of(context).pushReplacementNamed('/');
          }),
          buildListTile('Update Covid', Icons.query_stats, () {
            Navigator.of(context).pushReplacementNamed('/');
          }),
          buildListTile('Kontak Penting', Icons.ring_volume, () {
            Navigator.of(context).pushReplacementNamed('/');
          }),
          buildListTile('Kritik dan Saran', Icons.support_agent, () {
            Navigator.of(context).pushReplacementNamed('/');
          }),
          buildListTile('Logout', Icons.settings, () {
            Navigator.of(context).pushReplacementNamed(FiltersScreen.routeName);
          }),
        ],
      ),
    );
  }
}
