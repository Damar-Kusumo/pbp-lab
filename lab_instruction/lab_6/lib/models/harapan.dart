import 'package:flutter/material.dart';

class Harapan {
  final String author;
  final String harapan;
  final int like;
  final Icon x;
  final String status;
  final String tanggal;

  const Harapan({this.author,this.harapan,this.like,this.x,this.status,this.tanggal});

  // const Harapan({
  //   @required this.author,
  //   @required this.harapan,
  //   @required this.like,
  //   @required this.tanggal,
  // });
}
