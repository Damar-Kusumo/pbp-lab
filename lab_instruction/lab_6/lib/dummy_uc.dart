import 'package:flutter/material.dart';

import './models/harapan.dart';

const DUMMY_HARAPAN = const [
  Harapan(
    author: "Damar",
    harapan: "Ini adalah harapan", 
    like: 0,
    x: Icon(Icons.thumb_up, color:Colors.yellow),
    status: "Suka",
    tanggal: "18/11/2021",
  ),
  Harapan(
    author: "Damar",
    harapan: "Ini adalah harapan yang sangat panjang sekaliiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii", 
    like: 0,
    x: Icon(Icons.thumb_up, color:Colors.yellow),
    status: "Suka",
    tanggal: "18/11/2021",
  ),
  Harapan(
    author: "Muhammad Damar Kusumo",
    harapan: "Ini adalah harapan", 
    like: 0,
    x: Icon(Icons.thumb_up, color:Colors.yellow),
    status: "Suka",
    tanggal: "18/11/2021",
  ),
  Harapan(
    author: "Damar",
    harapan: "Ini adalah harapan", 
    like: 0,
    x: Icon(Icons.thumb_up, color:Colors.yellow),
    status: "Suka",
    tanggal: "18/11/2021",
  ),
  Harapan(
    author: "Damar",
    harapan: "Ini adalah harapan", 
    like: 0,
    x: Icon(Icons.thumb_up, color:Colors.yellow),
    status: "Suka",
    tanggal: "18/11/2021",
  ),

];