import 'package:flutter/material.dart';
import 'package:flutter_complete_guide/models/harapan.dart';
import 'package:flutter_complete_guide/screens/uc_harapan.dart';
import 'package:flutter_complete_guide/screens/uc_indo.dart';

import '../widgets/main_drawer.dart';
import './favorites_screen.dart';
import './categories_screen.dart';
import './uc_indo.dart';
import './uc_provinsi.dart';
import '../models/harapan.dart';

class TabsScreen extends StatefulWidget {
  final List<Harapan> listHarapan;

  TabsScreen(this.listHarapan);

  @override
  _TabsScreenState createState() => _TabsScreenState();
}

class _TabsScreenState extends State<TabsScreen> {
  List<Map<String, Object>> _pages;
  int _selectedPageIndex = 0;

  List<Harapan> get listHarapan => null;

  @override
  void initState() {
    _pages = [
      {
        'page': UCIndo(),
        'title': 'Update Harian Covid-19 Indonesia',
      },
      {
        'page': UCProv(),
        'title': 'Kotak Harapan',
      },
      {
        'page': UCHarapan(this.listHarapan),
        'title': 'Kotak Harapan',
      },
    ];
    super.initState();
  }

  void _selectPage(int index) {
    setState(() {
      _selectedPageIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Rumah Harapan🏠'),
        backgroundColor: Color.fromRGBO(89, 165, 216, 1),
      ),
      drawer: MainDrawer(),
      body: _pages[_selectedPageIndex]['page'],
      bottomNavigationBar: BottomNavigationBar(
        onTap: _selectPage,
        backgroundColor: Color.fromRGBO(89, 165, 216, 1),
        unselectedItemColor: Theme.of(context).accentColor,
        selectedItemColor: Colors.white,
        currentIndex: _selectedPageIndex,
        // type: BottomNavigationBarType.fixed,
        items: [
          BottomNavigationBarItem(
            backgroundColor: Color.fromRGBO(89, 165, 216, 1),
            icon: Icon(Icons.query_stats),
            title: Text('Harian Covid'),
          ),
          BottomNavigationBarItem(
            backgroundColor: Color.fromRGBO(89, 165, 216, 1),
            icon: Icon(Icons.list_alt),
            title: Text('Data Provinsi'),
          ),
          BottomNavigationBarItem(
            backgroundColor: Color.fromRGBO(89, 165, 216, 1),
            icon: Icon(Icons.comment),
            title: Text('Kotak Harapan'),
          ),
        ],
      ),
    );
  }
}
