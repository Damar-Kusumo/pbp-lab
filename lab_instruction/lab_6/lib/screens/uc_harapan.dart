import 'package:flutter/material.dart';

import '../models/harapan.dart';

class UCHarapan extends StatefulWidget {
  final List<Harapan> listHarapan;

  UCHarapan(this.listHarapan);

  @override
  _UCHarapanState createState() => _UCHarapanState(listHarapan);
}

class _UCHarapanState extends State<UCHarapan> {
  final List<Harapan> listHarapan;
  final _formKey = GlobalKey<FormState>();
  String suka = "Suka";
  Icon x = Icon(Icons.thumb_up,color: Colors.yellow,);
  int like = 0;
  String harapan = "Harapan default";
  DateTime waktu = DateTime.now();

  _UCHarapanState(this.listHarapan);

  void _incrementCounter() {
    setState(() {
      if (suka == "Suka") {
        suka = "Tidak Suka";
        x = Icon(Icons.thumb_down,color: Colors.yellow,);
        like++;
      } else {
        suka = "Suka";
        x = Icon(Icons.thumb_up,color: Colors.yellow,);
        like--;
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    if (listHarapan.isEmpty) {
      return Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            const Text( // ini nanti klo misal blm ada pke column biar center
              'Jadilah orang pertama yang menuliskan Harapan!',
              textAlign: TextAlign.center,
              style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 24, color: Color.fromRGBO(89, 165, 216, 1)),
            ),
            Padding(
            padding: EdgeInsets.all(16.0),
            child: Column(
              
            ),
            )
          ],
        ),
      );
    } else {
        return Scaffold(
          body: Padding(
            padding: EdgeInsets.all(16.0),
            child: Column(
          // mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(
                  'Kotak Harapan',
                  textAlign: TextAlign.center,
                  style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 28, color: Color.fromRGBO(89, 165, 216, 1)),
                ),
                Text(" ",),
                Container(
                  height: 50,
                  color: Color.fromRGBO(89, 165, 216, 1),
                  child: Padding(
                    padding: EdgeInsets.all(8),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Row(
                          // mainAxisSize: MainAxisSize.min,
                          children: <Widget> [
                            Row(
                          // mainAxisSize: MainAxisSize.min,
                              children: <Widget> [
                                Icon(
                                  Icons.person_pin,
                                  size: 32,
                                ),
                                Column(
                              // mainAxisSize: MainAxisSize.min,
                                  children: <Widget>[
                                    Text("Ini buat nama",
                                    style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 14),
                                    ),
                                    Text("   " + waktu.toLocal().toString(), 
                                        style: const TextStyle(fontSize: 14),
                                    ),
                              ],
                            )
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
            Container(
              height: 60,
              color: Color.fromRGBO(89, 165, 216, 1),
              child: Padding(
                padding: EdgeInsets.all(10),
                child: ListView(
                  // mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Text(harapan,style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 18),),
                  ],
                ),
              ),
            ),
            Container(
              height: 65,
              color: Color.fromRGBO(89, 165, 216, 1),
              child: Padding(
                padding: EdgeInsets.all(8),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget> [
                        FlatButton.icon(
                          onPressed: () {
                            _incrementCounter();
                          }, 
                          icon: x, 
                          label: Text(suka),
                          ),
                        // Icon(
                        //   Icons.thumb_up,
                        //   color: Colors.yellow,
                        // ),
                        Text(like.toString() + " Suka  ", style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 16)),
                      ],
                    ),
                  ],
                ),
              ),
            ),
            Text(" ",),
            Container(
              height: 50,
              color: Color.fromRGBO(89, 165, 216, 1),
              child: Padding(
                padding: EdgeInsets.all(8),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Row(
                      // mainAxisSize: MainAxisSize.min,
                      children: <Widget> [
                        Row(
                          // mainAxisSize: MainAxisSize.min,
                          children: <Widget> [
                            Icon(
                              Icons.person_pin,
                              size: 32,
                              ),
                            Column(
                              // mainAxisSize: MainAxisSize.min,
                              children: <Widget>[
                                Text("Ini buat nama",
                                style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 14),
                                ),
                                Text("   Ini buat Tanggal", 
                                style: const TextStyle(fontSize: 14),
                                ),
                              ],
                            )
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
            Container(
              height: 60,
              color: Color.fromRGBO(89, 165, 216, 1),
              child: Padding(
                padding: EdgeInsets.all(10),
                child: ListView(
                  // mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Text("Ini Adalah pesan yang disampaikan",style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 18),),
                  ],
                ),
              ),
            ),
            Container(
              height: 65,
              color: Color.fromRGBO(89, 165, 216, 1),
              child: Padding(
                padding: EdgeInsets.all(8),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget> [
                        FlatButton.icon(
                          onPressed: () {
                            _incrementCounter();
                          }, 
                          icon: x, 
                          label: Text(suka),
                          ),
                        // Icon(
                        //   Icons.thumb_up,
                        //   color: Colors.yellow,
                        // ),
                        Text(like.toString() + " Suka  ", style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 16)),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
    ),
      
      floatingActionButton: FloatingActionButton(
        backgroundColor: Color.fromRGBO(89, 165, 216, 1),
        onPressed: () {
          showDialog(
                context: context,
                builder: (BuildContext context) {
                  return AlertDialog(
                    content: Stack(
                      overflow: Overflow.visible,
                      children: <Widget>[
                        Positioned(
                          right: -50.0,
                          top: -50.0,
                          child: InkResponse(
                            onTap: () {
                              Navigator.of(context).pop();
                            },
                            child: CircleAvatar(
                              child: Icon(Icons.close),
                              backgroundColor: Colors.red,
                            ),
                          ),
                        ),
                        Form(
                          key: _formKey,
                          child: Column(
                            mainAxisSize: MainAxisSize.min,
                            children: <Widget>[
                              Padding(
                                padding: EdgeInsets.all(5.0),
                                child: TextFormField(
                                    maxLines: 4,
                                    decoration: InputDecoration(
                                      border: OutlineInputBorder(),
                                      labelText: 'Tuliskan Harapan Kamu disini',
                                    ),
                                    validator: (value){
                                      if (value == null || value.isEmpty) {
                                        return 'Please enter some text';
                                      }
                                      setState(() {
                                        harapan = value;
                                      });
                                      
                                      return null;
                                    },
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: RaisedButton(
                                  child: Text("Kirim"),
                                  onPressed: () {
                                    if (_formKey.currentState.validate()) {
                                      _formKey.currentState.save();
                                    }
                                  },
                                ),
                                )
                            ],
                          ),
                        ),
                      ],
                    ),
                  );
                });
        },
        child: const Icon(Icons.add),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
    }
      
    // else {
    //   return ListView.builder(
    //     itemBuilder: (ctx, index) {
    //       return MealItem(
    //         id: listHarapan[index].id,
    //         title: listHarapan[index].title,
    //         imageUrl: listHarapan[index].imageUrl,
    //         duration: listHarapan[index].duration,
    //         affordability: listHarapan[index].affordability,
    //         complexity: listHarapan[index].complexity,
    //       );
    //     },
    //     itemCount: listHarapan.length,
    //   );
    }
}
