import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/material/colors.dart';


class UCProv extends StatelessWidget {
  // const UCIndo({Key? key}) : super(key: key);
  final cardColor = Color.fromRGBO(89, 165, 216, 1);
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(16.0),
      child: Center (
        child: ListView(
          // mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              'Data per Provinsi',
              textAlign: TextAlign.center,
              style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 28, color:Color.fromRGBO(89, 165, 216, 1)),
            ),
            Text(" ",
            style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 16, color: Color.fromRGBO(89, 165, 216, 1)),
            ),
            
            Table(
              border: TableBorder.all(),
              columnWidths: const <int, TableColumnWidth>{
                0: FlexColumnWidth(),
                1: FlexColumnWidth(),
                2: FlexColumnWidth(),
                3: FlexColumnWidth(),
              },
              defaultVerticalAlignment: TableCellVerticalAlignment.middle,
              children: <TableRow>[
                TableRow(
                  decoration: const BoxDecoration(
                    color: Color.fromRGBO(89, 165, 216, 1),
                  ),
                  children: <Widget>[
                    Container(
                      height: 72,
                      child: Center ( 
                        child:Text(
                        'Provinsi',
                        textAlign: TextAlign.center,
                        style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 16, color: Colors.white),
                        ),
                      ),
                    ),
                    Container(
                      height: 72,
                      child: Center ( 
                        child:Text(
                        'Kasus Positif',
                        textAlign: TextAlign.center,
                        style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 16, color: Colors.white),
                        ),
                      ),
                    ),
                    Container(
                      height: 72,
                      child: Center ( 
                        child:Text(
                        'Kasus Sembuh',
                        textAlign: TextAlign.center,
                        style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 16, color: Colors.white),
                        ),
                      ),
                    ),
                    Container(
                      height: 72,
                      child: Center ( 
                        child:Text(
                        'Kasus Meninggal',
                        textAlign: TextAlign.center,
                        style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 16, color: Colors.white),
                        ),
                      ),
                    ),
                  ],
                ),
                TableRow(
                  children: <Widget>[
                    Container(
                      height: 72,
                      child: Center ( 
                        child:Text(
                        'DKI Jakarta',
                        textAlign: TextAlign.center,
                        style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 14, color: Color.fromRGBO(89, 165, 216, 1)),
                        ),
                      ),
                    ),
                    Container(
                      height: 72,
                      child: Center ( 
                        child:Text(
                        '406.205',
                        textAlign: TextAlign.center,
                        style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 14, color: Color.fromRGBO(89, 165, 216, 1)),
                        ),
                      ),
                    ),
                    Container(
                      height: 72,
                      child: Center ( 
                        child:Text(
                        '393.166',
                        textAlign: TextAlign.center,
                        style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 14, color: Color.fromRGBO(89, 165, 216, 1)),
                        ),
                      ),
                    ),
                    Container(
                      height: 72,
                      child: Center ( 
                        child:Text(
                        '6.625',
                        textAlign: TextAlign.center,
                        style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 14, color: Color.fromRGBO(89, 165, 216, 1)),
                        ),
                      ),
                    ),
                  ],
                ),
                TableRow(
                  children: <Widget>[
                    Container(
                      height: 72,
                      child: Center ( 
                        child:Text(
                        'Daerah Istimewa Yogyakarta',
                        textAlign: TextAlign.center,
                        style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 14, color: Color.fromRGBO(89, 165, 216, 1)),
                        ),
                      ),
                    ),
                    Container(
                      height: 72,
                      child: Center ( 
                        child:Text(
                        '38.703',
                        textAlign: TextAlign.center,
                        style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 14, color: Color.fromRGBO(89, 165, 216, 1)),
                        ),
                      ),
                    ),
                    Container(
                      height: 72,
                      child: Center ( 
                        child:Text(
                        '33.636',
                        textAlign: TextAlign.center,
                        style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 14, color: Color.fromRGBO(89, 165, 216, 1)),
                        ),
                      ),
                    ),
                    Container(
                      height: 72,
                      child: Center ( 
                        child:Text(
                        '948',
                        textAlign: TextAlign.center,
                        style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 14, color: Color.fromRGBO(89, 165, 216, 1)),
                        ),
                      ),
                    ),
                  ],
                ),
                TableRow(
                  children: <Widget>[
                    Container(
                      height: 72,
                      child: Center ( 
                        child:Text(
                        'Daerah Istimewa Yogyakarta',
                        textAlign: TextAlign.center,
                        style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 14, color: Color.fromRGBO(89, 165, 216, 1)),
                        ),
                      ),
                    ),
                    Container(
                      height: 72,
                      child: Center ( 
                        child:Text(
                        '38.703',
                        textAlign: TextAlign.center,
                        style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 14, color: Color.fromRGBO(89, 165, 216, 1)),
                        ),
                      ),
                    ),
                    Container(
                      height: 72,
                      child: Center ( 
                        child:Text(
                        '33.636',
                        textAlign: TextAlign.center,
                        style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 14, color: Color.fromRGBO(89, 165, 216, 1)),
                        ),
                      ),
                    ),
                    Container(
                      height: 72,
                      child: Center ( 
                        child:Text(
                        '948',
                        textAlign: TextAlign.center,
                        style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 14, color: Color.fromRGBO(89, 165, 216, 1)),
                        ),
                      ),
                    ),
                  ],
                ),
                TableRow(
                  children: <Widget>[
                    Container(
                      height: 72,
                      child: Center ( 
                        child:Text(
                        'Daerah Istimewa Yogyakarta',
                        textAlign: TextAlign.center,
                        style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 14, color: Color.fromRGBO(89, 165, 216, 1)),
                        ),
                      ),
                    ),
                    Container(
                      height: 72,
                      child: Center ( 
                        child:Text(
                        '38.703',
                        textAlign: TextAlign.center,
                        style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 14, color: Color.fromRGBO(89, 165, 216, 1)),
                        ),
                      ),
                    ),
                    Container(
                      height: 72,
                      child: Center ( 
                        child:Text(
                        '33.636',
                        textAlign: TextAlign.center,
                        style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 14, color: Color.fromRGBO(89, 165, 216, 1)),
                        ),
                      ),
                    ),
                    Container(
                      height: 72,
                      child: Center ( 
                        child:Text(
                        '948',
                        textAlign: TextAlign.center,
                        style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 14, color: Color.fromRGBO(89, 165, 216, 1)),
                        ),
                      ),
                    ),
                  ],
                ),
                TableRow(
                  children: <Widget>[
                    Container(
                      height: 72,
                      child: Center ( 
                        child:Text(
                        'Daerah Istimewa Yogyakarta',
                        textAlign: TextAlign.center,
                        style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 14, color: Color.fromRGBO(89, 165, 216, 1)),
                        ),
                      ),
                    ),
                    Container(
                      height: 72,
                      child: Center ( 
                        child:Text(
                        '38.703',
                        textAlign: TextAlign.center,
                        style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 14, color: Color.fromRGBO(89, 165, 216, 1)),
                        ),
                      ),
                    ),
                    Container(
                      height: 72,
                      child: Center ( 
                        child:Text(
                        '33.636',
                        textAlign: TextAlign.center,
                        style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 14, color: Color.fromRGBO(89, 165, 216, 1)),
                        ),
                      ),
                    ),
                    Container(
                      height: 72,
                      child: Center ( 
                        child:Text(
                        '948',
                        textAlign: TextAlign.center,
                        style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 14, color: Color.fromRGBO(89, 165, 216, 1)),
                        ),
                      ),
                    ),
                  ],
                ),
                TableRow(
                  children: <Widget>[
                    Container(
                      height: 72,
                      child: Center ( 
                        child:Text(
                        'Daerah Istimewa Yogyakarta',
                        textAlign: TextAlign.center,
                        style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 14, color: Color.fromRGBO(89, 165, 216, 1)),
                        ),
                      ),
                    ),
                    Container(
                      height: 72,
                      child: Center ( 
                        child:Text(
                        '38.703',
                        textAlign: TextAlign.center,
                        style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 14, color: Color.fromRGBO(89, 165, 216, 1)),
                        ),
                      ),
                    ),
                    Container(
                      height: 72,
                      child: Center ( 
                        child:Text(
                        '33.636',
                        textAlign: TextAlign.center,
                        style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 14, color: Color.fromRGBO(89, 165, 216, 1)),
                        ),
                      ),
                    ),
                    Container(
                      height: 72,
                      child: Center ( 
                        child:Text(
                        '948',
                        textAlign: TextAlign.center,
                        style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 14, color: Color.fromRGBO(89, 165, 216, 1)),
                        ),
                      ),
                    ),
                  ],
                ),
                TableRow(
                  children: <Widget>[
                    Container(
                      height: 72,
                      child: Center ( 
                        child:Text(
                        'Daerah Istimewa Yogyakarta',
                        textAlign: TextAlign.center,
                        style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 14, color: Color.fromRGBO(89, 165, 216, 1)),
                        ),
                      ),
                    ),
                    Container(
                      height: 72,
                      child: Center ( 
                        child:Text(
                        '38.703',
                        textAlign: TextAlign.center,
                        style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 14, color: Color.fromRGBO(89, 165, 216, 1)),
                        ),
                      ),
                    ),
                    Container(
                      height: 72,
                      child: Center ( 
                        child:Text(
                        '33.636',
                        textAlign: TextAlign.center,
                        style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 14, color: Color.fromRGBO(89, 165, 216, 1)),
                        ),
                      ),
                    ),
                    Container(
                      height: 72,
                      child: Center ( 
                        child:Text(
                        '948',
                        textAlign: TextAlign.center,
                        style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 14, color: Color.fromRGBO(89, 165, 216, 1)),
                        ),
                      ),
                    ),
                  ],
                ),
                TableRow(
                  children: <Widget>[
                    Container(
                      height: 72,
                      child: Center ( 
                        child:Text(
                        'Daerah Istimewa Yogyakarta',
                        textAlign: TextAlign.center,
                        style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 14, color: Color.fromRGBO(89, 165, 216, 1)),
                        ),
                      ),
                    ),
                    Container(
                      height: 72,
                      child: Center ( 
                        child:Text(
                        '38.703',
                        textAlign: TextAlign.center,
                        style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 14, color: Color.fromRGBO(89, 165, 216, 1)),
                        ),
                      ),
                    ),
                    Container(
                      height: 72,
                      child: Center ( 
                        child:Text(
                        '33.636',
                        textAlign: TextAlign.center,
                        style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 14, color: Color.fromRGBO(89, 165, 216, 1)),
                        ),
                      ),
                    ),
                    Container(
                      height: 72,
                      child: Center ( 
                        child:Text(
                        '948',
                        textAlign: TextAlign.center,
                        style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 14, color: Color.fromRGBO(89, 165, 216, 1)),
                        ),
                      ),
                    ),
                  ],
                ),
              ],

            ),
          ],
        ),
    ),
    );
  }
}