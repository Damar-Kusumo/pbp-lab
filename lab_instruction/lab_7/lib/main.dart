import 'package:flutter/material.dart';

import './dummy_uc.dart';
import './screens/tabs_screen.dart';
import './models/harapan.dart';

void main() => runApp(UpdateCovid());

class UpdateCovid extends StatefulWidget {
  @override
  _UpdateCovidState createState() => _UpdateCovidState();
}

class _UpdateCovidState extends State<UpdateCovid> {
  List<Harapan> _listHarapan = DUMMY_HARAPAN;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Update Covid Indonesia',
      
      // theme: ThemeData(
      //   primarySwatch: Colors.blue,
      //   accentColor: Colors.indigo,
      //   canvasColor: Color.fromRGBO(173, 232, 244, 1),
      //   fontFamily: 'Raleway',
      //   textTheme: ThemeData.light().textTheme.copyWith(
      //       bodyText1: TextStyle(
      //         color: Color.fromRGBO(20, 51, 51, 1),
      //       ),
      //       bodyText2: TextStyle(
      //         color: Color.fromRGBO(20, 51, 51, 1),
      //       ),
      //       headline6: TextStyle(
      //         fontSize: 20,
      //         fontFamily: 'RobotoCondensed',
      //         fontWeight: FontWeight.bold,
      //       )),
      // ),
      home: TabsScreen(_listHarapan),
      // initialRoute: '/', // default is '/'
      // routes: {
      //   '/': (ctx) => TabsScreen(_favoriteMeals),
      //   CategoryMealsScreen.routeName: (ctx) =>
      //       CategoryMealsScreen(_availableMeals),
      //   MealDetailScreen.routeName: (ctx) => MealDetailScreen(_toggleFavorite, _isMealFavorite),
      //   FiltersScreen.routeName: (ctx) => FiltersScreen(_filters, _setFilters),
      // },
      // onGenerateRoute: (settings) {
      //   print(settings.arguments);
      // },
      // onUnknownRoute: (settings) {
      //   return MaterialPageRoute(
      //     builder: (ctx) => CategoriesScreen(),
      //   );
      // },
    );
  }
}
