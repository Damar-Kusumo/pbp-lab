import 'package:flutter/material.dart';

import 'harapan.dart';

const DUMMY_HARAPAN = const [
  Harapan(
    // author: "Damar",
    harapan: "Ini adalah harapan", 
    like: 1,
    x: Icon(Icons.thumb_up, color:Colors.yellow),
    status: "Suka",
    tanggal: "18/11/2021",
  ),
  Harapan(
    // author: "Damar",
    harapan: "Ini adalah harapan yang sangat panjang sekaliiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii", 
    like: 2,
    x: Icon(Icons.thumb_up, color:Colors.yellow),
    status: "Suka",
    tanggal: "18/11/2021",
  ),
  Harapan(
    // author: "Muhammad Damar Kusumo",
    harapan: "Ini adalah harapan", 
    like: 3,
    x: Icon(Icons.thumb_up, color:Colors.yellow),
    status: "Suka",
    tanggal: "18/11/2021",
  ),
  Harapan(
    // author: "Damar",
    harapan: "Ini adalah harapan", 
    like: 4,
    x: Icon(Icons.thumb_up, color:Colors.yellow),
    status: "Suka",
    tanggal: "18/11/2021",
  ),
  Harapan(
    // author: "Damar",
    harapan: "Ini adalah harapan", 
    like: 5,
    x: Icon(Icons.thumb_up, color:Colors.yellow),
    status: "Suka",
    tanggal: "18/11/2021",
  ),

];