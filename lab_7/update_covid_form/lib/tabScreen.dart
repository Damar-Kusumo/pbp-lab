import 'package:flutter/material.dart';
import 'harapan.dart';
import 'main.dart';
import 'dummy.dart';
import 'drawer.dart';

class TabsScreen extends StatefulWidget {
  final List<Harapan> listHarapan;

  TabsScreen(this.listHarapan);

  @override
  _TabsScreenState createState() => _TabsScreenState();
}

class _TabsScreenState extends State<TabsScreen> {
  late List<Map<String, Widget>> _pages;
  int _selectedPageIndex = 0;

  // List<Harapan> get listHarapan => null;

  @override
  void initState() {
    _pages = [
      {
        'page': UCIndo(),
      },
      {
        'page': UCProv(),
      },
      {
        'page': UCHarapan(DUMMY_HARAPAN),
      },
    ];
    super.initState();
  }

  void _selectPage(int index) {
    setState(() {
      _selectedPageIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Rumah Harapan🏠'),
        backgroundColor: const Color.fromRGBO(89, 165, 216, 1),
      ),
      drawer: MainDrawer(),
      body: _pages[_selectedPageIndex]['page'],
      bottomNavigationBar: BottomNavigationBar(
        onTap: _selectPage,
        backgroundColor: Color.fromRGBO(89, 165, 216, 1),
        unselectedItemColor: Color.fromRGBO(2, 62, 128, 1),
        selectedItemColor: Colors.white,
        currentIndex: _selectedPageIndex,
        // type: BottomNavigationBarType.fixed,
        items: [
          BottomNavigationBarItem(
            icon: Icon(Icons.query_stats),
            title: Text('Harian Covid'),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.list_alt),
            title: Text('Data Provinsi'),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.comment),
            title: Text('Kotak Harapan'),
          ),
        ],
      ),
    );
  }
}