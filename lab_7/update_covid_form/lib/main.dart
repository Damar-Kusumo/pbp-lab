import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:update_covid_form/cardHarapan.dart';
import 'harapan.dart';
import 'dummy.dart';
import 'tabScreen.dart';
import 'drawer.dart';
import 'cardHarapan.dart';

void main() => runApp(UpdateCovid());

class UpdateCovid extends StatefulWidget {
  @override
  _UpdateCovidState createState() => _UpdateCovidState();
}

class _UpdateCovidState extends State<UpdateCovid> {
  List<Harapan> _listHarapan = DUMMY_HARAPAN;
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Update Covid Indonesia',
      theme: ThemeData(
        canvasColor: Color.fromRGBO(173, 232, 244, 1),
        fontFamily: 'Raleway',
      ),
      home: TabsScreen(_listHarapan),
    );
  }
}

class UCIndo extends StatelessWidget {
  UCIndo({Key? key}) : super(key: key);
  final cardColor = Color.fromRGBO(89, 165, 216, 1);
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(16.0),
      child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              'Update Harian Covid-19 Indonesia',
              textAlign: TextAlign.center,
              style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 28, color: Color.fromRGBO(89, 165, 216, 1)),
            ),
            Text(" ",style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 32, color: Color.fromRGBO(89, 165, 216, 1)),),
            Container(
              height: 100,
              child: Card(
                color: cardColor,
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    const ListTile(
                      title: Text('Kasus Aktif 😷',
                      textAlign: TextAlign.center,
                      style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 24, color: Colors.white),
                      ),
                    ),
                    Text(
                      '4.250.855',
                      style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 18, color: Colors.white),
                      ),
                  ],
                ),
              ),
            ),
            Container(
              height: 100,
              child: Card(
                color: cardColor,
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    const ListTile(
                      title: Text('Sembuh 💉',
                      textAlign: TextAlign.center,
                      style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 24, color: Colors.white ),
                      ),
                    ),
                    Text(
                      '4.098.178',
                      style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 18, color: Colors.white),
                      ),
                  ],
                ),
              ),
            ),
            Container(
              height: 100,
              child: Card(
                color: cardColor,
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    const ListTile(
                      title: Text('Meninggal ☠️',
                      textAlign: TextAlign.center,
                      style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 24, color: Colors.white),
                      ),
                    ),
                    Text(
                      '143.659',
                      style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 18, color: Colors.white),
                      ),
                  ],
                ),
              ),
            ),
          ],
        ),
    );
  }
}

class UCProv extends StatelessWidget {
  UCProv({Key? key}) : super(key: key);
  final cardColor = Color.fromRGBO(89, 165, 216, 1);
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(16.0),
      child: Center (
        child: ListView(
          // mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              'Data per Provinsi',
              textAlign: TextAlign.center,
              style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 28, color:Color.fromRGBO(89, 165, 216, 1)),
            ),
            Text(" ",
            style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 16, color: Color.fromRGBO(89, 165, 216, 1)),
            ),
            
            Table(
              border: TableBorder.all(),
              columnWidths: const <int, TableColumnWidth>{
                0: FlexColumnWidth(),
                1: FlexColumnWidth(),
                2: FlexColumnWidth(),
                3: FlexColumnWidth(),
              },
              defaultVerticalAlignment: TableCellVerticalAlignment.middle,
              children: <TableRow>[
                TableRow(
                  decoration: const BoxDecoration(
                    color: Color.fromRGBO(89, 165, 216, 1),
                  ),
                  children: <Widget>[
                    Container(
                      height: 72,
                      child: Center ( 
                        child:Text(
                        'Provinsi',
                        textAlign: TextAlign.center,
                        style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 16, color: Colors.white),
                        ),
                      ),
                    ),
                    Container(
                      height: 72,
                      child: Center ( 
                        child:Text(
                        'Kasus Positif',
                        textAlign: TextAlign.center,
                        style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 16, color: Colors.white),
                        ),
                      ),
                    ),
                    Container(
                      height: 72,
                      child: Center ( 
                        child:Text(
                        'Kasus Sembuh',
                        textAlign: TextAlign.center,
                        style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 16, color: Colors.white),
                        ),
                      ),
                    ),
                    Container(
                      height: 72,
                      child: Center ( 
                        child:Text(
                        'Kasus Meninggal',
                        textAlign: TextAlign.center,
                        style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 16, color: Colors.white),
                        ),
                      ),
                    ),
                  ],
                ),
                TableRow(
                  children: <Widget>[
                    Container(
                      height: 72,
                      child: Center ( 
                        child:Text(
                        'DKI Jakarta',
                        textAlign: TextAlign.center,
                        style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 14, color: Color.fromRGBO(89, 165, 216, 1)),
                        ),
                      ),
                    ),
                    Container(
                      height: 72,
                      child: Center ( 
                        child:Text(
                        '406.205',
                        textAlign: TextAlign.center,
                        style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 14, color: Color.fromRGBO(89, 165, 216, 1)),
                        ),
                      ),
                    ),
                    Container(
                      height: 72,
                      child: Center ( 
                        child:Text(
                        '393.166',
                        textAlign: TextAlign.center,
                        style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 14, color: Color.fromRGBO(89, 165, 216, 1)),
                        ),
                      ),
                    ),
                    Container(
                      height: 72,
                      child: Center ( 
                        child:Text(
                        '6.625',
                        textAlign: TextAlign.center,
                        style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 14, color: Color.fromRGBO(89, 165, 216, 1)),
                        ),
                      ),
                    ),
                  ],
                ),
                TableRow(
                  children: <Widget>[
                    Container(
                      height: 72,
                      child: Center ( 
                        child:Text(
                        'Daerah Istimewa Yogyakarta',
                        textAlign: TextAlign.center,
                        style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 14, color: Color.fromRGBO(89, 165, 216, 1)),
                        ),
                      ),
                    ),
                    Container(
                      height: 72,
                      child: Center ( 
                        child:Text(
                        '38.703',
                        textAlign: TextAlign.center,
                        style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 14, color: Color.fromRGBO(89, 165, 216, 1)),
                        ),
                      ),
                    ),
                    Container(
                      height: 72,
                      child: Center ( 
                        child:Text(
                        '33.636',
                        textAlign: TextAlign.center,
                        style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 14, color: Color.fromRGBO(89, 165, 216, 1)),
                        ),
                      ),
                    ),
                    Container(
                      height: 72,
                      child: Center ( 
                        child:Text(
                        '948',
                        textAlign: TextAlign.center,
                        style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 14, color: Color.fromRGBO(89, 165, 216, 1)),
                        ),
                      ),
                    ),
                  ],
                ),
                TableRow(
                  children: <Widget>[
                    Container(
                      height: 72,
                      child: Center ( 
                        child:Text(
                        'Daerah Istimewa Yogyakarta',
                        textAlign: TextAlign.center,
                        style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 14, color: Color.fromRGBO(89, 165, 216, 1)),
                        ),
                      ),
                    ),
                    Container(
                      height: 72,
                      child: Center ( 
                        child:Text(
                        '38.703',
                        textAlign: TextAlign.center,
                        style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 14, color: Color.fromRGBO(89, 165, 216, 1)),
                        ),
                      ),
                    ),
                    Container(
                      height: 72,
                      child: Center ( 
                        child:Text(
                        '33.636',
                        textAlign: TextAlign.center,
                        style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 14, color: Color.fromRGBO(89, 165, 216, 1)),
                        ),
                      ),
                    ),
                    Container(
                      height: 72,
                      child: Center ( 
                        child:Text(
                        '948',
                        textAlign: TextAlign.center,
                        style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 14, color: Color.fromRGBO(89, 165, 216, 1)),
                        ),
                      ),
                    ),
                  ],
                ),
                TableRow(
                  children: <Widget>[
                    Container(
                      height: 72,
                      child: Center ( 
                        child:Text(
                        'Daerah Istimewa Yogyakarta',
                        textAlign: TextAlign.center,
                        style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 14, color: Color.fromRGBO(89, 165, 216, 1)),
                        ),
                      ),
                    ),
                    Container(
                      height: 72,
                      child: Center ( 
                        child:Text(
                        '38.703',
                        textAlign: TextAlign.center,
                        style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 14, color: Color.fromRGBO(89, 165, 216, 1)),
                        ),
                      ),
                    ),
                    Container(
                      height: 72,
                      child: Center ( 
                        child:Text(
                        '33.636',
                        textAlign: TextAlign.center,
                        style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 14, color: Color.fromRGBO(89, 165, 216, 1)),
                        ),
                      ),
                    ),
                    Container(
                      height: 72,
                      child: Center ( 
                        child:Text(
                        '948',
                        textAlign: TextAlign.center,
                        style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 14, color: Color.fromRGBO(89, 165, 216, 1)),
                        ),
                      ),
                    ),
                  ],
                ),
                TableRow(
                  children: <Widget>[
                    Container(
                      height: 72,
                      child: Center ( 
                        child:Text(
                        'Daerah Istimewa Yogyakarta',
                        textAlign: TextAlign.center,
                        style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 14, color: Color.fromRGBO(89, 165, 216, 1)),
                        ),
                      ),
                    ),
                    Container(
                      height: 72,
                      child: Center ( 
                        child:Text(
                        '38.703',
                        textAlign: TextAlign.center,
                        style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 14, color: Color.fromRGBO(89, 165, 216, 1)),
                        ),
                      ),
                    ),
                    Container(
                      height: 72,
                      child: Center ( 
                        child:Text(
                        '33.636',
                        textAlign: TextAlign.center,
                        style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 14, color: Color.fromRGBO(89, 165, 216, 1)),
                        ),
                      ),
                    ),
                    Container(
                      height: 72,
                      child: Center ( 
                        child:Text(
                        '948',
                        textAlign: TextAlign.center,
                        style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 14, color: Color.fromRGBO(89, 165, 216, 1)),
                        ),
                      ),
                    ),
                  ],
                ),
                TableRow(
                  children: <Widget>[
                    Container(
                      height: 72,
                      child: Center ( 
                        child:Text(
                        'Daerah Istimewa Yogyakarta',
                        textAlign: TextAlign.center,
                        style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 14, color: Color.fromRGBO(89, 165, 216, 1)),
                        ),
                      ),
                    ),
                    Container(
                      height: 72,
                      child: Center ( 
                        child:Text(
                        '38.703',
                        textAlign: TextAlign.center,
                        style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 14, color: Color.fromRGBO(89, 165, 216, 1)),
                        ),
                      ),
                    ),
                    Container(
                      height: 72,
                      child: Center ( 
                        child:Text(
                        '33.636',
                        textAlign: TextAlign.center,
                        style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 14, color: Color.fromRGBO(89, 165, 216, 1)),
                        ),
                      ),
                    ),
                    Container(
                      height: 72,
                      child: Center ( 
                        child:Text(
                        '948',
                        textAlign: TextAlign.center,
                        style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 14, color: Color.fromRGBO(89, 165, 216, 1)),
                        ),
                      ),
                    ),
                  ],
                ),
                TableRow(
                  children: <Widget>[
                    Container(
                      height: 72,
                      child: Center ( 
                        child:Text(
                        'Daerah Istimewa Yogyakarta',
                        textAlign: TextAlign.center,
                        style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 14, color: Color.fromRGBO(89, 165, 216, 1)),
                        ),
                      ),
                    ),
                    Container(
                      height: 72,
                      child: Center ( 
                        child:Text(
                        '38.703',
                        textAlign: TextAlign.center,
                        style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 14, color: Color.fromRGBO(89, 165, 216, 1)),
                        ),
                      ),
                    ),
                    Container(
                      height: 72,
                      child: Center ( 
                        child:Text(
                        '33.636',
                        textAlign: TextAlign.center,
                        style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 14, color: Color.fromRGBO(89, 165, 216, 1)),
                        ),
                      ),
                    ),
                    Container(
                      height: 72,
                      child: Center ( 
                        child:Text(
                        '948',
                        textAlign: TextAlign.center,
                        style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 14, color: Color.fromRGBO(89, 165, 216, 1)),
                        ),
                      ),
                    ),
                  ],
                ),
                TableRow(
                  children: <Widget>[
                    Container(
                      height: 72,
                      child: Center ( 
                        child:Text(
                        'Daerah Istimewa Yogyakarta',
                        textAlign: TextAlign.center,
                        style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 14, color: Color.fromRGBO(89, 165, 216, 1)),
                        ),
                      ),
                    ),
                    Container(
                      height: 72,
                      child: Center ( 
                        child:Text(
                        '38.703',
                        textAlign: TextAlign.center,
                        style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 14, color: Color.fromRGBO(89, 165, 216, 1)),
                        ),
                      ),
                    ),
                    Container(
                      height: 72,
                      child: Center ( 
                        child:Text(
                        '33.636',
                        textAlign: TextAlign.center,
                        style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 14, color: Color.fromRGBO(89, 165, 216, 1)),
                        ),
                      ),
                    ),
                    Container(
                      height: 72,
                      child: Center ( 
                        child:Text(
                        '948',
                        textAlign: TextAlign.center,
                        style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 14, color: Color.fromRGBO(89, 165, 216, 1)),
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ],
        ),
    ),
    );
  }
}

class UCHarapan extends StatefulWidget {
  final List<Harapan> listHarapan;
  final _formKey = GlobalKey<FormState>();
 
  UCHarapan(this.listHarapan, {Key? key}) : super(key: key);

  @override
  State<UCHarapan> createState() => _UCHarapanState();
}

class _UCHarapanState extends State<UCHarapan> {
  // final List<Harapan> listHarapan;
  String suka = "Suka";
  Icon x = Icon(Icons.thumb_up,color: Colors.yellow,);
  int like = 0;
  String harapan = "Harapan default";
  DateTime waktu = DateTime.now();

  @override
  Widget build(BuildContext context) {
    if (widget.listHarapan.isEmpty) {
      return Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            const Text( // ini nanti klo misal blm ada pke column biar center
              'Jadilah orang pertama yang menuliskan Harapan!',
              textAlign: TextAlign.center,
              style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 24, color: Color.fromRGBO(89, 165, 216, 1)),
            ),
            Padding(
            padding: EdgeInsets.all(16.0),
            child: Column(
              
            ),
            )
          ],
        ),
      );
    } else {
      return Scaffold(
        body: Padding(
          padding: EdgeInsets.all(16.0),
          child: ListView(
          // mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                  'Kotak Harapan',
                  textAlign: TextAlign.center,
                  style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 28, color: Color.fromRGBO(89, 165, 216, 1)),
                ),
              // Text(" ",),
              // Form(
              //   key: widget._formKey,
              //   child: Column(
              //     mainAxisSize: MainAxisSize.min,
              //     children: <Widget>[
              //       Padding(
              //         padding: EdgeInsets.all(5.0),
              //         child: TextFormField(
              //           maxLines: 4,
              //           decoration: InputDecoration(
              //             border: OutlineInputBorder(),
              //             labelText: 'Tuliskan Harapan Kamu disini',
              //           ),
              //           validator: (value){
              //             if (value == null || value.isEmpty) {
              //                 return 'Silahkan masukan harapan anda';
              //             }
              //             setState(() {
              //               harapan = value;
              //             });                  
              //             return null;
              //           },
              //         ),
              //       ),
              //       Row(
              //         mainAxisSize: MainAxisSize.min,
              //         children: <Widget> [ 
              //           RaisedButton(
              //             child: Text("Kirim"),
              //             color: Color.fromRGBO(89, 165, 216, 1),
              //             onPressed: () {
              //               if (widget._formKey.currentState!.validate()) {
              //                 harapan = harapan;
              //                 waktu = DateTime.now();
              //                 print(harapan);
              //                 print(waktu);
              //               }
              //             }        
              //           ),
              //         ],
              //       ),
              //     ],
              //   ),
              // ),
    
              Text(" ",),
              ListView.builder(
                shrinkWrap: true,
                itemCount: widget.listHarapan.length,
                itemBuilder: (ctx,index) {
                  return CardHarapan(
                    widget.listHarapan[index].harapan, 
                    widget.listHarapan[index].like, 
                    widget.listHarapan[index].x, 
                    widget.listHarapan[index].status, 
                    widget.listHarapan[index].tanggal);
                },),
                ],
              ),
            ),
            floatingActionButton: FloatingActionButton(
              backgroundColor: Color.fromRGBO(2, 62, 128, 1),
              onPressed: () {
                showDialog(
                      context: context,
                      builder: (BuildContext context) {
                        return AlertDialog(
                          content: Stack(
                            overflow: Overflow.visible,
                            children: <Widget>[
                              Positioned(
                                right: -50.0,
                                top: -50.0,
                                child: InkResponse(
                                  onTap: () {
                                    Navigator.of(context).pop();
                                  },
                                  child: CircleAvatar(
                                    child: Icon(Icons.close),
                                    backgroundColor: Colors.red,
                                  ),
                                ),
                              ),
                              Form(
                                key: widget._formKey,
                                child: Column(
                                  mainAxisSize: MainAxisSize.min,
                                  children: <Widget>[
                                    Padding(
                                      padding: EdgeInsets.all(5.0),
                                      child: TextFormField(
                                          maxLines: 4,
                                          decoration: InputDecoration(
                                            border: OutlineInputBorder(),
                                            labelText: 'Tuliskan Harapan Kamu disini',
                                          ),
                                          validator: (value){
                                            if (value == null || value.isEmpty) {
                                              return 'Silahkan masukan harapan anda';
                                            }
                                            setState(() {
                                              harapan = value;
                                            });
                                            
                                            return null;
                                          },
                                      ),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: RaisedButton(
                                        child: Text("Kirim"),
                                        onPressed: () {
                                        if (widget._formKey.currentState!.validate()) {
                                          harapan = harapan;
                                          waktu = DateTime.now();
                                          print(harapan);
                                          print(waktu);
                                          Navigator.of(context).pop();
                                        }
                                      }
                                        ,
                                      ),
                                      )
                                  ],
                                ),
                              ),
                            ],
                          ),
                        );
                      });
              },
              child: const Icon(Icons.add),
            ),
          );
    //     return Scaffold(
    //       body: Padding(
    //         padding: EdgeInsets.all(16.0),
    //         child: ListView(
    //       // mainAxisAlignment: MainAxisAlignment.center,
    //           children: <Widget>[
    //             Text(
    //               'Kotak Harapan',
    //               textAlign: TextAlign.center,
    //               style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 28, color: Color.fromRGBO(89, 165, 216, 1)),
    //             ),
    //             Text(" ",),
    //             Container(
    //               height: 50,
    //               color: Color.fromRGBO(89, 165, 216, 1),
    //               child: Padding(
    //                 padding: EdgeInsets.all(8),
    //                 child: Column(
    //                   mainAxisAlignment: MainAxisAlignment.center,
    //                   children: <Widget>[
    //                     Row(
    //                       // mainAxisSize: MainAxisSize.min,
    //                       children: <Widget> [
    //                         Row(
    //                       // mainAxisSize: MainAxisSize.min,
    //                           children: <Widget> [
    //                             Icon(
    //                               Icons.person_pin,
    //                               size: 32,
    //                             ),
    //                             Column(
    //                           // mainAxisSize: MainAxisSize.min,
    //                               children: <Widget>[
    //                                 Text("Ini buat nama",
    //                                 style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 14),
    //                                 ),
    //                                 Text("   " + DateFormat('kk:mm  dd-MM-yyyy').format(waktu), 
    //                                     style: const TextStyle(fontSize: 14),
    //                                 ),
    //                           ],
    //                         )
    //                       ],
    //                     ),
    //                   ],
    //                 ),
    //               ],
    //             ),
    //           ),
    //         ),
    //         Container(
    //           height: 60,
    //           color: Color.fromRGBO(89, 165, 216, 1),
    //           child: Padding(
    //             padding: EdgeInsets.all(10),
    //             child: ListView(
    //               // mainAxisSize: MainAxisSize.min,
    //               children: <Widget>[
    //                 Text(harapan,style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 18),),
    //               ],
    //             ),
    //           ),
    //         ),
    //         Container(
    //           height: 65,
    //           color: Color.fromRGBO(89, 165, 216, 1),
    //           child: Padding(
    //             padding: EdgeInsets.all(8),
    //             child: Column(
    //               mainAxisAlignment: MainAxisAlignment.center,
    //               children: <Widget>[
    //                 Row(
    //                   mainAxisAlignment: MainAxisAlignment.spaceBetween,
    //                   children: <Widget> [
    //                     FlatButton.icon(
    //                       onPressed: () {
    //                         _incrementCounter();
    //                       }, 
    //                       icon: x, 
    //                       label: Text(suka),
    //                       ),
    //                     // Icon(
    //                     //   Icons.thumb_up,
    //                     //   color: Colors.yellow,
    //                     // ),
    //                     Text(like.toString() + " Suka  ", style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 16)),
    //                   ],
    //                 ),
    //               ],
    //             ),
    //           ),
    //         ),
    //         Text(" ",),
    //         Container(
    //           height: 50,
    //           color: Color.fromRGBO(89, 165, 216, 1),
    //           child: Padding(
    //             padding: EdgeInsets.all(8),
    //             child: Column(
    //               mainAxisAlignment: MainAxisAlignment.center,
    //               children: <Widget>[
    //                 Row(
    //                   // mainAxisSize: MainAxisSize.min,
    //                   children: <Widget> [
    //                     Row(
    //                       // mainAxisSize: MainAxisSize.min,
    //                       children: <Widget> [
    //                         Icon(
    //                           Icons.person_pin,
    //                           size: 32,
    //                           ),
    //                         Column(
    //                           // mainAxisSize: MainAxisSize.min,
    //                           children: <Widget>[
    //                             Text("Ini buat nama",
    //                             style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 14),
    //                             ),
    //                             Text("   Ini buat Tanggal", 
    //                             style: const TextStyle(fontSize: 14),
    //                             ),
    //                           ],
    //                         )
    //                       ],
    //                     ),
    //                   ],
    //                 ),
    //               ],
    //             ),
    //           ),
    //         ),
    //         Container(
    //           height: 60,
    //           color: Color.fromRGBO(89, 165, 216, 1),
    //           child: Padding(
    //             padding: EdgeInsets.all(10),
    //             child: ListView(
    //               // mainAxisSize: MainAxisSize.min,
    //               children: <Widget>[
    //                 Text("Ini Adalah pesan yang disampaikan",style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 18),),
    //               ],
    //             ),
    //           ),
    //         ),
    //         Container(
    //           height: 65,
    //           color: Color.fromRGBO(89, 165, 216, 1),
    //           child: Padding(
    //             padding: EdgeInsets.all(8),
    //             child: Column(
    //               mainAxisAlignment: MainAxisAlignment.center,
    //               children: <Widget>[
    //                 Row(
    //                   mainAxisAlignment: MainAxisAlignment.spaceBetween,
    //                   children: <Widget> [
    //                     FlatButton.icon(
    //                       onPressed: () {
    //                         _incrementCounter();
    //                       }, 
    //                       icon: x, 
    //                       label: Text(suka),
    //                       ),
    //                     // Icon(
    //                     //   Icons.thumb_up,
    //                     //   color: Colors.yellow,
    //                     // ),
    //                     Text(like.toString() + " Suka  ", style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 16)),
    //                   ],
    //                 ),
    //               ],
    //             ),
    //           ),
    //         ),
    //       ],
    //     ),
    // ),
      
    //   floatingActionButton: FloatingActionButton(
    //     backgroundColor: Color.fromRGBO(89, 165, 216, 1),
    //     onPressed: () {
    //       showDialog(
    //             context: context,
    //             builder: (BuildContext context) {
    //               return AlertDialog(
    //                 content: Stack(
    //                   overflow: Overflow.visible,
    //                   children: <Widget>[
    //                     Positioned(
    //                       right: -50.0,
    //                       top: -50.0,
    //                       child: InkResponse(
    //                         onTap: () {
    //                           Navigator.of(context).pop();
    //                         },
    //                         child: CircleAvatar(
    //                           child: Icon(Icons.close),
    //                           backgroundColor: Colors.red,
    //                         ),
    //                       ),
    //                     ),
    //                     Form(
    //                       key: _formKey,
    //                       child: Column(
    //                         mainAxisSize: MainAxisSize.min,
    //                         children: <Widget>[
    //                           Padding(
    //                             padding: EdgeInsets.all(5.0),
    //                             child: TextFormField(
    //                                 maxLines: 4,
    //                                 decoration: InputDecoration(
    //                                   border: OutlineInputBorder(),
    //                                   labelText: 'Tuliskan Harapan Kamu disini',
    //                                 ),
    //                                 validator: (value){
    //                                   if (value == null || value.isEmpty) {
    //                                     return 'Silahkan masukan harapan anda';
    //                                   }
    //                                   setState(() {
    //                                     harapan = value;
    //                                   });
                                      
    //                                   return null;
    //                                 },
    //                             ),
    //                           ),
    //                           Padding(
    //                             padding: const EdgeInsets.all(8.0),
    //                             child: RaisedButton(
    //                               child: Text("Kirim"),
    //                               onPressed: () {
    //                               if (_formKey.currentState!.validate()) {
    //                                 harapan = harapan;
    //                                 waktu = DateTime.now();
    //                                 Navigator.of(context).pop();
    //                               }
    //                             }
    //                               ,
    //                             ),
    //                             )
    //                         ],
    //                       ),
    //                     ),
    //                   ],
    //                 ),
    //               );
    //             });
    //     },
    //     child: const Icon(Icons.add),
    //   ), // This trailing comma makes auto-formatting nicer for build methods.
    // );
    }
      
    // else {
    //   return ListView.builder(
    //     itemBuilder: (ctx, index) {
    //       return MealItem(
    //         id: listHarapan[index].id,
    //         title: listHarapan[index].title,
    //         imageUrl: listHarapan[index].imageUrl,
    //         duration: listHarapan[index].duration,
    //         affordability: listHarapan[index].affordability,
    //         complexity: listHarapan[index].complexity,
    //       );
    //     },
    //     itemCount: listHarapan.length,
    //   );
    }
}