// ignore_for_file: file_names

import 'package:flutter/material.dart';

import 'harapan.dart';
import 'main.dart';
import 'dummy.dart';

// class CardHarapan extends StatefulWidget {
//   final String harapan;
//   final int like;
//   final Icon x;
//   final String status;
//   final String tanggal;
//   const CardHarapan({ Key?key,
//   required this.harapan,
//   required this.like,
//   required this.x,
//   required this.status,
//   required this.tanggal }) : super(key: key);

//   @override
//   // ignore: no_logic_in_create_state
//   State<CardHarapan> createState() => _CardHarapanState(harapan, like, x, status, tanggal);
// }

class CardHarapan extends StatelessWidget {
  String harapan;
  int like;
  Icon x;
  String status;
  String tanggal;

  CardHarapan(
    this.harapan,
    this.like,
    this.x,
    this.status,
    this.tanggal);

  // void _incrementCounter() {
  //   setState(() {
  //     if (status == "Suka") {
  //       status = "Tidak Suka";
  //       x = Icon(Icons.thumb_down,color: Colors.yellow,);
  //       like++;
  //     } else {
  //       status = "Suka";
  //       x = Icon(Icons.thumb_up,color: Colors.yellow,);
  //       like--;
  //     }
  //   });
  // }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
          // mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Container(
                  height: 50,
                  color: Color.fromRGBO(89, 165, 216, 1),
                  child: Padding(
                    padding: EdgeInsets.all(8),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Row(
                          // mainAxisSize: MainAxisSize.min,
                          children: <Widget> [
                            Row(
                          // mainAxisSize: MainAxisSize.min,
                              children: <Widget> [
                                Icon(
                                  Icons.person_pin,
                                  size: 32,
                                ),
                                Column(
                              // mainAxisSize: MainAxisSize.min,
                                  children: <Widget>[
                                    Text("Ini buat nama",
                                    style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 14),
                                    ),
                                    Text("   " + tanggal, 
                                        style: const TextStyle(fontSize: 14),
                                    ),
                              ],
                            )
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
            Container(
              height: 60,
              color: Color.fromRGBO(89, 165, 216, 1),
              child: Padding(
                padding: EdgeInsets.all(10),
                child: ListView(
                  // mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Text(harapan,style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 18),),
                  ],
                ),
              ),
            ),
            Container(
              height: 65,
              color: Color.fromRGBO(89, 165, 216, 1),
              child: Padding(
                padding: EdgeInsets.all(8),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget> [
                        FlatButton.icon(
                          onPressed: () {
                            // _incrementCounter();
                          }, 
                          icon: x, 
                          label: Text(status),
                          ),
                        // Icon(
                        //   Icons.thumb_up,
                        //   color: Colors.yellow,
                        // ),
                        Text(like.toString() + " Suka  ", style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 16)),
                      ],
                    ),
                  ],
                ),
              ),
            ),
            Text(" ",),
          ],
    );
    

    // InkWell(
    //   onTap: () => selectMeal(context),
    //   child: Card(
    //     shape: RoundedRectangleBorder(
    //       borderRadius: BorderRadius.circular(15),
    //     ),
    //     elevation: 4,
    //     margin: EdgeInsets.all(10),
    //     child: Column(
    //       children: <Widget>[
    //         Stack(
    //           children: <Widget>[
    //             ClipRRect(
    //               borderRadius: BorderRadius.only(
    //                 topLeft: Radius.circular(15),
    //                 topRight: Radius.circular(15),
    //               ),
    //               child: Image.network(
    //                 imageUrl,
    //                 height: 250,
    //                 width: double.infinity,
    //                 fit: BoxFit.cover,
    //               ),
    //             ),
    //             Positioned(
    //               bottom: 20,
    //               right: 10,
    //               child: Container(
    //                 width: 300,
    //                 color: Colors.black54,
    //                 padding: EdgeInsets.symmetric(
    //                   vertical: 5,
    //                   horizontal: 20,
    //                 ),
    //                 child: Text(
    //                   title,
    //                   style: TextStyle(
    //                     fontSize: 26,
    //                     color: Colors.white,
    //                   ),
    //                   softWrap: true,
    //                   overflow: TextOverflow.fade,
    //                 ),
    //               ),
    //             )
    //           ],
    //         ),
    //         Padding(
    //           padding: EdgeInsets.all(20),
    //           child: Row(
    //             mainAxisAlignment: MainAxisAlignment.spaceAround,
    //             children: <Widget>[
    //               Row(
    //                 children: <Widget>[
    //                   Icon(
    //                     Icons.schedule,
    //                   ),
    //                   SizedBox(
    //                     width: 6,
    //                   ),
    //                   Text('$duration min'),
    //                 ],
    //               ),
    //               Row(
    //                 children: <Widget>[
    //                   Icon(
    //                     Icons.work,
    //                   ),
    //                   SizedBox(
    //                     width: 6,
    //                   ),
    //                   Text(complexityText),
    //                 ],
    //               ),
    //               Row(
    //                 children: <Widget>[
    //                   Icon(
    //                     Icons.attach_money,
    //                   ),
    //                   SizedBox(
    //                     width: 6,
    //                   ),
    //                   Text(affordabilityText),
    //                 ],
    //               ),
    //             ],
    //           ),
    //         ),
    //       ],
    //     ),
    //   ),
    // );
  }
}

