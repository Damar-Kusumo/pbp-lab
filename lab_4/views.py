from django.shortcuts import render
from django.shortcuts import redirect
from lab_2.models import Note
from .form import NoteForm
from django.contrib.auth.decorators import login_required


# Create your views here.
@login_required(login_url='/admin/login/')
def index(request):
    notes = Note.objects.all().values()  
    response = {'notes': notes}
    return render(request, 'lab4_index.html', response)

@login_required(login_url='/admin/login/')
def add_note(request):
    # create object of form
    form = NoteForm(request.POST)
    notes = Note.objects.all().values()
    response = {'notes': notes}
    if form.is_valid() and request.method == 'POST':
        # save the form data to model
        form.save()
        return redirect('/lab-4', response)
  
    return render(request, "lab4_form.html", {'form' : form})

@login_required(login_url='/admin/login/')
def note_list(request):
    notes = Note.objects.all().values()  
    response = {'notes': notes}
    return render(request, 'lab4_note_list.html', response)