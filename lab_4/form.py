from django import forms
from lab_2.models import Note

class NoteForm(forms.ModelForm):
    class Meta:
        model = Note
        fields = '__all__'      # Include all fields di Friend
        widgets = {
            'To': forms.TextInput(attrs={'class': 'form-control'}),
            'From': forms.TextInput(attrs={'class': 'form-control'}),
            'Title': forms.TextInput(attrs={'class': 'form-control', 'placeholder' : 'Max 30 Character'}),
            'Message': forms.Textarea(attrs={'class': 'form-control','placeholder' : 'Write your message here'}),
        }
    error_messages = {
        'required':'Please Type'
    }
    


	


