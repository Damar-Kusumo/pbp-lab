from django import forms
from lab_1.models import Friend

class DateInput(forms.DateInput):
    input_type = 'date'

class FriendForm(forms.ModelForm):
    class Meta:
        model = Friend
        fields = '__all__'      # Include all fields di Friend
        widgets = {
            'dob': DateInput(),    # ref https://stackoverflow.com/questions/3367091/whats-the-cleanest-simplest-to-get-running-datepicker-in-django 
        }
    error_messages = {
        'required':'Please Type'
    }
    


	


