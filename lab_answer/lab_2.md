Nama: Muhammad Damar Kusumo
NPM: 2006532903

# 1. Apakah perbedaan antara JSON dan XML?

Keduanya sama-sama diperuntukan untuk melihat data dengan bentuk yang berbeda. Biasanya penggunaan JSON dan XML ditujukan apabila kita hanya ingin mengambil informasi berdasarkan data yang tersimpan di dalam web saja.

Perbedaan dari keduanya adalah dari sisi pada saat menampilkan datanya. JSON (JavaScript Object Notation) menampilkan object dengan format dictionary ("key" : "value") key menunjukan nama atribute dan valuenya menunjukan isi atribute dari object tersebut, tiap tiap object merupakan value dari fields, sehingga tiap satu object merupakan 1 dictionary yang berisi atribute dan valuenya. 

Ex JSON:
[{"model": "lab_2.note", "pk": 1, "fields": {"To": "Kamu", "From": "Aku", "Title": "Hallo", "Message": "Hallo ,apa kabar?"}}, {"model": "lab_2.note", "pk": 2, "fields": {"To": "Abin", "From": "Damar", "Title": "Podcast", "Message": "Kapan lanjut bang?"}}]

Sementara itu XML (Extensible markup language) menampilakan object menggunakan tag dan atribut seperti yang ada pada HTML.

Ex XML: 

<django-objects version="1.0"> 
    <object model="lab_2.note" pk="1">         <!--> ini menyatakan object pertama dan dibawahnya berisi apa-apa saja yang ada di object pertama (To,From,Title,Message) <-->
        <field name="To" type="CharField">Kamu</field> 
        <field name="From" type="CharField">Aku</field>
        <field name="Title" type="CharField">Hallo</field>
        <field name="Message" type="CharField">Hallo ,apa kabar?</field>
    </object>
    <object model="lab_2.note" pk="2">         <!--> ini menyatakan object selanjutnya dan dibawahnya berisi apa-apa saja yang ada di object pertama (To,From,Title,Message) <-->
        <field name="To" type="CharField">Abin</field>
        <field name="From" type="CharField">Damar</field>
        <field name="Title" type="CharField">Podcast</field>
        <field name="Message" type="CharField">Kapan lanjut bang?</field>
    </object>
</django-objects>

Perbedaan lainnya berdasarkan sumber yang saya baca, XML lebih aman daripada JSON, juga apabila dilihat dari contoh di atas XML dapat memunculkan namespaces tidak dengan JSON yang hanya berdasarkan keynya.

# 2. Apakah perbedaan antara HTML (Hyper Text Markup Language) dan XML?

Keduanya sama-sama merupakan markup language, yang mana keduanya memiliki tags dan atribute yang menandakan sebuah elements. Yang membedakan adalah tags pada html menunjukan dan mempengaruhi bagaimana tampilannya saat dijalankan, sementara XML tagsnya tidak mempengaruhi tampilannya hanya mendreskripsikan data yang ada. HTML yang mana juga sebagai hypertext bisa bersifat static yang mana tidak mempedulikan database dan hanya menampilkannya, sebaliknya XML pasti bersifat dynamic yang mana akan terus menyesuaikan dengan database yang ada. Tags pada HTML terbatas sesuai dengan perkembangnya, sebaliknya tags pada HTML dapat di extend sesuai dengan kebutuhan dalam menyajikan data. Pada kesimpulannya HTML lebih digunakan untuk menampilkan tampilan sesuai dengan keinginan pengguna, sementara XML lebih digunakan untuk penyimpanan dan penyajian data yang lebih bisa mudah dipahami oleh manusia.


# Referensi
https://www.geeksforgeeks.org/difference-between-json-and-xml/ 
https://www.guru99.com/json-vs-xml-difference.html
https://www.geeksforgeeks.org/html-vs-xml/
https://www.guru99.com/xml-vs-html-difference.html